<?php
namespace App\Repository;

use App\Entity\Presupuesto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PresupuestoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Presupuesto::class);
    }

    /**
     * @param $presupuesto
     * @return Presupuesto
     */
    public function findOnePresubyId($presupuesto): Presupuesto
    {
        // automatically knows to select Products
        // the "p" is an alias you'll use in the rest of the query
        $qb = $this->createQueryBuilder('p')
            ->leftJoin('p.items','items')
            ->addSelect('items')
            ->where('p.id = :presupuesto')
            ->andWhere('items.idPadre is null')
            ->setParameter('presupuesto', $presupuesto)
            ->orderBy('items.idPadre', 'ASC')
            ->getQuery();

        //return $qb->execute();                   ->andWhere('items.codigoparticular is null')      ->andWhere('items.idPadre is null')
        return $qb->getSingleResult();
        // to get just one result:
        //return $qb->setMaxResults(1)->getOneOrNullResult();
    }

     /**
     * @param $cliente, $obra
     * @return string[]
     */
    public function findVersiones($cliente, $obra): Array
    {
        $qb = $this->createQueryBuilder('p')
            ->select('p.version')
            ->where('p.cliente = :cliente')
            ->andWhere('p.obra = :obra')
            ->setParameter('cliente', $cliente)
            ->setParameter('obra', $obra)
            ->getQuery();

        return $qb->execute();
        //return $qb->getSingleResult();
        // to get just one result:
        //return $qb->setMaxResults(1)->getOneOrNullResult();
    }
}
?>