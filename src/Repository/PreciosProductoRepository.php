<?php
namespace App\Repository;

use App\Entity\PreciosProducto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PreciosProductoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PreciosProducto::class);
    }

    /**
     * @param $presupuesto
     * @return PreciosProducto[]
     */
    public function findAllWS(): array
    {
        // automatically knows to select Products
        // the "p" is an alias you'll use in the rest of the query
       
        $qb = $this
    ->createQueryBuilder('b')
    ->select('b.fecha')->setMaxResults(1)->getQuery();

        return $qb->execute();

        // to get just one result:
        // $product = $qb->setMaxResults(1)->getOneOrNullResult();
    }
}
?>