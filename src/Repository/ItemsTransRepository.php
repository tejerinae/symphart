<?php
namespace App\Repository;

use App\Entity\ItemsTrans;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ItemsTransRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ItemsTrans::class);
    }

    /**
     * @param $presupuesto
     * @return ItemsTrans[]
     */
    public function findAllItemsbypresu($presupuesto): array
    {
        // automatically knows to select Products
        // the "p" is an alias you'll use in the rest of the query
        $qb = $this->createQueryBuilder('i')
            ->where('i.codigoparticular is null')
            ->andWhere('i.idPadre is null')
            ->andWhere('i.idPresupuesto = :presupuesto')
            ->setParameter('presupuesto', $presupuesto)
            ->orderBy('i.idPadre', 'ASC')
            ->getQuery();

        return $qb->execute();

        // to get just one result:
        // $product = $qb->setMaxResults(1)->getOneOrNullResult();
    }
    
}
?>