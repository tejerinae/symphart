<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Filtros
 *
 * @ORM\Table(name="filtros")
 * @ORM\Entity
 */
class Filtros
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="campo", type="string", length=20, nullable=false)
     */
    private $campo;

    /**
     * @var int
     *
     * @ORM\Column(name="id_tabcharts", type="integer", nullable=false)
     */
    private $idTabcharts;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCampo(): ?string
    {
        return $this->campo;
    }

    public function setCampo(string $campo): self
    {
        $this->campo = $campo;

        return $this;
    }

    public function getIdTabcharts(): ?int
    {
        return $this->idTabcharts;
    }

    public function setIdTabcharts(int $idTabcharts): self
    {
        $this->idTabcharts = $idTabcharts;

        return $this;
    }


}
