<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TabCharts
 *
 * @ORM\Table(name="tab_charts")
 * @ORM\Entity
 */
class TabCharts
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="chart", type="string", length=50, nullable=false)
     */
    private $chart;

    /**
     * @var string
     *
     * @ORM\Column(name="connection", type="string", length=100, nullable=false)
     */
    private $connection;

    /**
     * @var string
     *
     * @ORM\Column(name="scriptSql", type="text", length=65535, nullable=false)
     */
    private $scriptsql;

    /**
     * @var string
     *
     * @ORM\Column(name="divToChart", type="text", length=65535, nullable=false)
     */
    private $divtochart;

    /**
     * @var string
     *
     * @ORM\Column(name="chartTable", type="string", length=25, nullable=false)
     */
    private $charttable;

    /**
     * @var string
     *
     * @ORM\Column(name="chartFunction", type="string", length=25, nullable=false)
     */
    private $chartfunction;

    /**
     * @var int
     *
     * @ORM\Column(name="id_tab", type="integer", nullable=false)
     */
    private $idTab;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=200, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="tabDrillDown", type="string", length=50, nullable=false)
     */
    private $tabdrilldown;

    /**
     * @var string
     *
     * @ORM\Column(name="tooltipText", type="text", length=65535, nullable=false)
     */
    private $tooltiptext;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChart(): ?string
    {
        return $this->chart;
    }

    public function setChart(string $chart): self
    {
        $this->chart = $chart;

        return $this;
    }

    public function getConnection(): ?string
    {
        return $this->connection;
    }

    public function setConnection(string $connection): self
    {
        $this->connection = $connection;

        return $this;
    }

    public function getScriptsql(): ?string
    {
        return $this->scriptsql;
    }

    public function setScriptsql(string $scriptsql): self
    {
        $this->scriptsql = $scriptsql;

        return $this;
    }

    public function getDivtochart(): ?string
    {
        return $this->divtochart;
    }

    public function setDivtochart(string $divtochart): self
    {
        $this->divtochart = $divtochart;

        return $this;
    }

    public function getCharttable(): ?string
    {
        return $this->charttable;
    }

    public function setCharttable(string $charttable): self
    {
        $this->charttable = $charttable;

        return $this;
    }

    public function getChartfunction(): ?string
    {
        return $this->chartfunction;
    }

    public function setChartfunction(string $chartfunction): self
    {
        $this->chartfunction = $chartfunction;

        return $this;
    }

    public function getIdTab(): ?int
    {
        return $this->idTab;
    }

    public function setIdTab(int $idTab): self
    {
        $this->idTab = $idTab;

        return $this;
    }

    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getTabdrilldown(): ?string
    {
        return $this->tabdrilldown;
    }

    public function setTabdrilldown(string $tabdrilldown): self
    {
        $this->tabdrilldown = $tabdrilldown;

        return $this;
    }

    public function getTooltiptext(): ?string
    {
        return $this->tooltiptext;
    }

    public function setTooltiptext(string $tooltiptext): self
    {
        $this->tooltiptext = $tooltiptext;

        return $this;
    }


}
