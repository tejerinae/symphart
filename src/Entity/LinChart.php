<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LinChart
 *
 * @ORM\Table(name="lin_chart", uniqueConstraints={@ORM\UniqueConstraint(name="nombre", columns={"nombre"})})
 * @ORM\Entity
 */
class LinChart
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tpo", type="string", length=50, nullable=true)
     */
    private $tpo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=55, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="backgroundColor", type="string", length=255, nullable=true)
     */
    private $backgroundcolor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="chartArea", type="string", length=255, nullable=true)
     */
    private $chartarea;

    /**
     * @var string|null
     *
     * @ORM\Column(name="colors", type="string", length=255, nullable=true)
     */
    private $colors;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fontSize", type="string", length=255, nullable=true)
     */
    private $fontsize;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fontName", type="string", length=255, nullable=true)
     */
    private $fontname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="height", type="string", length=255, nullable=true)
     */
    private $height;

    /**
     * @var string|null
     *
     * @ORM\Column(name="legend", type="string", length=255, nullable=true)
     */
    private $legend;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titlePosition", type="string", length=255, nullable=true)
     */
    private $titleposition;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titleTextStyle", type="string", length=255, nullable=true)
     */
    private $titletextstyle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vAxis", type="string", length=255, nullable=true)
     */
    private $vaxis;

    /**
     * @var int|null
     *
     * @ORM\Column(name="vAxis_baseline", type="integer", nullable=true)
     */
    private $vaxisBaseline;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vAxis_baselineColor", type="string", length=50, nullable=true)
     */
    private $vaxisBaselinecolor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vAxis_direction", type="string", length=2, nullable=true)
     */
    private $vaxisDirection;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vAxis_format", type="string", length=200, nullable=true)
     */
    private $vaxisFormat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vAxis_gridlines", type="string", length=250, nullable=true)
     */
    private $vaxisGridlines;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vAxis_textStyle", type="string", length=250, nullable=true)
     */
    private $vaxisTextstyle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vAxis_titleTextStyle", type="string", length=250, nullable=true)
     */
    private $vaxisTitletextstyle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="width", type="string", length=20, nullable=true)
     */
    private $width;

    /**
     * @var string|null
     *
     * @ORM\Column(name="axisTitlesPosition", type="string", length=10, nullable=true)
     */
    private $axistitlesposition;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hAxis", type="string", length=255, nullable=true)
     */
    private $haxis;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hAxis_textStyle", type="string", length=250, nullable=true)
     */
    private $haxisTextstyle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hAxis_titleTextStyle", type="string", length=250, nullable=true)
     */
    private $haxisTitletextstyle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="backgroundColor_stroke", type="string", length=20, nullable=true)
     */
    private $backgroundcolorStroke;

    /**
     * @var int|null
     *
     * @ORM\Column(name="backgroundColor_strokeWidth", type="integer", nullable=true)
     */
    private $backgroundcolorStrokewidth;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hAxis_direction", type="string", length=3, nullable=true)
     */
    private $haxisDirection;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hAxis_textPosition", type="string", length=5, nullable=true)
     */
    private $haxisTextposition;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hAxis_slantedText", type="string", length=10, nullable=true)
     */
    private $haxisSlantedtext;

    /**
     * @var int|null
     *
     * @ORM\Column(name="hAxis_slantedTextAngle", type="integer", nullable=true)
     */
    private $haxisSlantedtextangle;

    /**
     * @var int|null
     *
     * @ORM\Column(name="hAxis_maxAlternation", type="integer", nullable=true)
     */
    private $haxisMaxalternation;

    /**
     * @var string|null
     *
     * @ORM\Column(name="legend_textStyle", type="string", length=245, nullable=true)
     */
    private $legendTextstyle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="reverseCategories", type="string", length=20, nullable=true)
     */
    private $reversecategories;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tooltip_showColorCode", type="string", length=10, nullable=true)
     */
    private $tooltipShowcolorcode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tooltip_textStyle", type="string", length=250, nullable=true)
     */
    private $tooltipTextstyle;

    /**
     * @var int|null
     *
     * @ORM\Column(name="animation_duration", type="integer", nullable=true)
     */
    private $animationDuration;

    /**
     * @var string|null
     *
     * @ORM\Column(name="animation_easing", type="string", length=50, nullable=true)
     */
    private $animationEasing;

    /**
     * @var string|null
     *
     * @ORM\Column(name="seriesType", type="string", length=50, nullable=true)
     */
    private $seriestype;

    /**
     * @var string|null
     *
     * @ORM\Column(name="series", type="string", length=200, nullable=true)
     */
    private $series;

    /**
     * @var string|null
     *
     * @ORM\Column(name="curveType", type="string", length=20, nullable=true)
     */
    private $curvetype;

    /**
     * @var string|null
     *
     * @ORM\Column(name="interpolateNulls", type="string", length=10, nullable=true)
     */
    private $interpolatenulls;

    /**
     * @var int|null
     *
     * @ORM\Column(name="lineWidth", type="integer", nullable=true)
     */
    private $linewidth;

    /**
     * @var int|null
     *
     * @ORM\Column(name="pointSize", type="integer", nullable=true)
     */
    private $pointsize;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTpo(): ?string
    {
        return $this->tpo;
    }

    public function setTpo(?string $tpo): self
    {
        $this->tpo = $tpo;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getBackgroundcolor(): ?string
    {
        return $this->backgroundcolor;
    }

    public function setBackgroundcolor(?string $backgroundcolor): self
    {
        $this->backgroundcolor = $backgroundcolor;

        return $this;
    }

    public function getChartarea(): ?string
    {
        return $this->chartarea;
    }

    public function setChartarea(?string $chartarea): self
    {
        $this->chartarea = $chartarea;

        return $this;
    }

    public function getColors(): ?string
    {
        return $this->colors;
    }

    public function setColors(?string $colors): self
    {
        $this->colors = $colors;

        return $this;
    }

    public function getFontsize(): ?string
    {
        return $this->fontsize;
    }

    public function setFontsize(?string $fontsize): self
    {
        $this->fontsize = $fontsize;

        return $this;
    }

    public function getFontname(): ?string
    {
        return $this->fontname;
    }

    public function setFontname(?string $fontname): self
    {
        $this->fontname = $fontname;

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->height;
    }

    public function setHeight(?string $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getLegend(): ?string
    {
        return $this->legend;
    }

    public function setLegend(?string $legend): self
    {
        $this->legend = $legend;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getTitleposition(): ?string
    {
        return $this->titleposition;
    }

    public function setTitleposition(?string $titleposition): self
    {
        $this->titleposition = $titleposition;

        return $this;
    }

    public function getTitletextstyle(): ?string
    {
        return $this->titletextstyle;
    }

    public function setTitletextstyle(?string $titletextstyle): self
    {
        $this->titletextstyle = $titletextstyle;

        return $this;
    }

    public function getVaxis(): ?string
    {
        return $this->vaxis;
    }

    public function setVaxis(?string $vaxis): self
    {
        $this->vaxis = $vaxis;

        return $this;
    }

    public function getVaxisBaseline(): ?int
    {
        return $this->vaxisBaseline;
    }

    public function setVaxisBaseline(?int $vaxisBaseline): self
    {
        $this->vaxisBaseline = $vaxisBaseline;

        return $this;
    }

    public function getVaxisBaselinecolor(): ?string
    {
        return $this->vaxisBaselinecolor;
    }

    public function setVaxisBaselinecolor(?string $vaxisBaselinecolor): self
    {
        $this->vaxisBaselinecolor = $vaxisBaselinecolor;

        return $this;
    }

    public function getVaxisDirection(): ?string
    {
        return $this->vaxisDirection;
    }

    public function setVaxisDirection(?string $vaxisDirection): self
    {
        $this->vaxisDirection = $vaxisDirection;

        return $this;
    }

    public function getVaxisFormat(): ?string
    {
        return $this->vaxisFormat;
    }

    public function setVaxisFormat(?string $vaxisFormat): self
    {
        $this->vaxisFormat = $vaxisFormat;

        return $this;
    }

    public function getVaxisGridlines(): ?string
    {
        return $this->vaxisGridlines;
    }

    public function setVaxisGridlines(?string $vaxisGridlines): self
    {
        $this->vaxisGridlines = $vaxisGridlines;

        return $this;
    }

    public function getVaxisTextstyle(): ?string
    {
        return $this->vaxisTextstyle;
    }

    public function setVaxisTextstyle(?string $vaxisTextstyle): self
    {
        $this->vaxisTextstyle = $vaxisTextstyle;

        return $this;
    }

    public function getVaxisTitletextstyle(): ?string
    {
        return $this->vaxisTitletextstyle;
    }

    public function setVaxisTitletextstyle(?string $vaxisTitletextstyle): self
    {
        $this->vaxisTitletextstyle = $vaxisTitletextstyle;

        return $this;
    }

    public function getWidth(): ?string
    {
        return $this->width;
    }

    public function setWidth(?string $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getAxistitlesposition(): ?string
    {
        return $this->axistitlesposition;
    }

    public function setAxistitlesposition(?string $axistitlesposition): self
    {
        $this->axistitlesposition = $axistitlesposition;

        return $this;
    }

    public function getHaxis(): ?string
    {
        return $this->haxis;
    }

    public function setHaxis(?string $haxis): self
    {
        $this->haxis = $haxis;

        return $this;
    }

    public function getHaxisTextstyle(): ?string
    {
        return $this->haxisTextstyle;
    }

    public function setHaxisTextstyle(?string $haxisTextstyle): self
    {
        $this->haxisTextstyle = $haxisTextstyle;

        return $this;
    }

    public function getHaxisTitletextstyle(): ?string
    {
        return $this->haxisTitletextstyle;
    }

    public function setHaxisTitletextstyle(?string $haxisTitletextstyle): self
    {
        $this->haxisTitletextstyle = $haxisTitletextstyle;

        return $this;
    }

    public function getBackgroundcolorStroke(): ?string
    {
        return $this->backgroundcolorStroke;
    }

    public function setBackgroundcolorStroke(?string $backgroundcolorStroke): self
    {
        $this->backgroundcolorStroke = $backgroundcolorStroke;

        return $this;
    }

    public function getBackgroundcolorStrokewidth(): ?int
    {
        return $this->backgroundcolorStrokewidth;
    }

    public function setBackgroundcolorStrokewidth(?int $backgroundcolorStrokewidth): self
    {
        $this->backgroundcolorStrokewidth = $backgroundcolorStrokewidth;

        return $this;
    }

    public function getHaxisDirection(): ?string
    {
        return $this->haxisDirection;
    }

    public function setHaxisDirection(?string $haxisDirection): self
    {
        $this->haxisDirection = $haxisDirection;

        return $this;
    }

    public function getHaxisTextposition(): ?string
    {
        return $this->haxisTextposition;
    }

    public function setHaxisTextposition(?string $haxisTextposition): self
    {
        $this->haxisTextposition = $haxisTextposition;

        return $this;
    }

    public function getHaxisSlantedtext(): ?string
    {
        return $this->haxisSlantedtext;
    }

    public function setHaxisSlantedtext(?string $haxisSlantedtext): self
    {
        $this->haxisSlantedtext = $haxisSlantedtext;

        return $this;
    }

    public function getHaxisSlantedtextangle(): ?int
    {
        return $this->haxisSlantedtextangle;
    }

    public function setHaxisSlantedtextangle(?int $haxisSlantedtextangle): self
    {
        $this->haxisSlantedtextangle = $haxisSlantedtextangle;

        return $this;
    }

    public function getHaxisMaxalternation(): ?int
    {
        return $this->haxisMaxalternation;
    }

    public function setHaxisMaxalternation(?int $haxisMaxalternation): self
    {
        $this->haxisMaxalternation = $haxisMaxalternation;

        return $this;
    }

    public function getLegendTextstyle(): ?string
    {
        return $this->legendTextstyle;
    }

    public function setLegendTextstyle(?string $legendTextstyle): self
    {
        $this->legendTextstyle = $legendTextstyle;

        return $this;
    }

    public function getReversecategories(): ?string
    {
        return $this->reversecategories;
    }

    public function setReversecategories(?string $reversecategories): self
    {
        $this->reversecategories = $reversecategories;

        return $this;
    }

    public function getTooltipShowcolorcode(): ?string
    {
        return $this->tooltipShowcolorcode;
    }

    public function setTooltipShowcolorcode(?string $tooltipShowcolorcode): self
    {
        $this->tooltipShowcolorcode = $tooltipShowcolorcode;

        return $this;
    }

    public function getTooltipTextstyle(): ?string
    {
        return $this->tooltipTextstyle;
    }

    public function setTooltipTextstyle(?string $tooltipTextstyle): self
    {
        $this->tooltipTextstyle = $tooltipTextstyle;

        return $this;
    }

    public function getAnimationDuration(): ?int
    {
        return $this->animationDuration;
    }

    public function setAnimationDuration(?int $animationDuration): self
    {
        $this->animationDuration = $animationDuration;

        return $this;
    }

    public function getAnimationEasing(): ?string
    {
        return $this->animationEasing;
    }

    public function setAnimationEasing(?string $animationEasing): self
    {
        $this->animationEasing = $animationEasing;

        return $this;
    }

    public function getSeriestype(): ?string
    {
        return $this->seriestype;
    }

    public function setSeriestype(?string $seriestype): self
    {
        $this->seriestype = $seriestype;

        return $this;
    }

    public function getSeries(): ?string
    {
        return $this->series;
    }

    public function setSeries(?string $series): self
    {
        $this->series = $series;

        return $this;
    }

    public function getCurvetype(): ?string
    {
        return $this->curvetype;
    }

    public function setCurvetype(?string $curvetype): self
    {
        $this->curvetype = $curvetype;

        return $this;
    }

    public function getInterpolatenulls(): ?string
    {
        return $this->interpolatenulls;
    }

    public function setInterpolatenulls(?string $interpolatenulls): self
    {
        $this->interpolatenulls = $interpolatenulls;

        return $this;
    }

    public function getLinewidth(): ?int
    {
        return $this->linewidth;
    }

    public function setLinewidth(?int $linewidth): self
    {
        $this->linewidth = $linewidth;

        return $this;
    }

    public function getPointsize(): ?int
    {
        return $this->pointsize;
    }

    public function setPointsize(?int $pointsize): self
    {
        $this->pointsize = $pointsize;

        return $this;
    }


}
