<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PvtChart
 *
 * @ORM\Table(name="pvt_chart")
 * @ORM\Entity
 */
class PvtChart
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=150, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="headerRowIndexes", type="string", length=50, nullable=false)
     */
    private $headerrowindexes;

    /**
     * @var string
     *
     * @ORM\Column(name="headerColIndexes", type="string", length=50, nullable=false)
     */
    private $headercolindexes;

    /**
     * @var string
     *
     * @ORM\Column(name="filterIndexes", type="string", length=50, nullable=false)
     */
    private $filterindexes;

    /**
     * @var string
     *
     * @ORM\Column(name="dataColumnIndex", type="string", length=50, nullable=false)
     */
    private $datacolumnindex;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getHeaderrowindexes(): ?string
    {
        return $this->headerrowindexes;
    }

    public function setHeaderrowindexes(string $headerrowindexes): self
    {
        $this->headerrowindexes = $headerrowindexes;

        return $this;
    }

    public function getHeadercolindexes(): ?string
    {
        return $this->headercolindexes;
    }

    public function setHeadercolindexes(string $headercolindexes): self
    {
        $this->headercolindexes = $headercolindexes;

        return $this;
    }

    public function getFilterindexes(): ?string
    {
        return $this->filterindexes;
    }

    public function setFilterindexes(string $filterindexes): self
    {
        $this->filterindexes = $filterindexes;

        return $this;
    }

    public function getDatacolumnindex(): ?string
    {
        return $this->datacolumnindex;
    }

    public function setDatacolumnindex(string $datacolumnindex): self
    {
        $this->datacolumnindex = $datacolumnindex;

        return $this;
    }


}
