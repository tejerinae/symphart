<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Grid
 *
 * @ORM\Table(name="grid")
 * @ORM\Entity
 */
class Grid
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=false)
     */
    private $nombre;

    /**
     * @var int
     *
     * @ORM\Column(name="rowNum", type="integer", nullable=false, options={"default"="100"})
     */
    private $rownum = '100';

    /**
     * @var string
     *
     * @ORM\Column(name="rowList", type="string", length=100, nullable=false, options={"default"="[100,200,300]"})
     */
    private $rowlist = '[100,200,300]';

    /**
     * @var string
     *
     * @ORM\Column(name="height", type="string", length=100, nullable=false, options={"default"="'auto'"})
     */
    private $height = '\'auto\'';

    /**
     * @var string
     *
     * @ORM\Column(name="width", type="string", length=100, nullable=false)
     */
    private $width;

    /**
     * @var string
     *
     * @ORM\Column(name="sortname", type="string", length=200, nullable=false)
     */
    private $sortname;

    /**
     * @var string
     *
     * @ORM\Column(name="viewrecords", type="string", length=10, nullable=false, options={"default"="true"})
     */
    private $viewrecords = 'true';

    /**
     * @var string
     *
     * @ORM\Column(name="sortorder", type="string", length=10, nullable=false)
     */
    private $sortorder;

    /**
     * @var string
     *
     * @ORM\Column(name="caption", type="string", length=250, nullable=false)
     */
    private $caption;

    /**
     * @var string
     *
     * @ORM\Column(name="grouping", type="string", length=10, nullable=false)
     */
    private $grouping;

    /**
     * @var int
     *
     * @ORM\Column(name="col_model", type="integer", nullable=false)
     */
    private $colModel;

    /**
     * @var int
     *
     * @ORM\Column(name="col_name", type="integer", nullable=false)
     */
    private $colName;

    /**
     * @var string
     *
     * @ORM\Column(name="groupingView", type="text", length=65535, nullable=false)
     */
    private $groupingview;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ParamDetailGrid", type="string", length=25, nullable=true)
     */
    private $paramdetailgrid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ParamDetailSubGrid", type="string", length=25, nullable=true)
     */
    private $paramdetailsubgrid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sqlDetail", type="text", length=65535, nullable=true)
     */
    private $sqldetail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sqlSubDetail", type="text", length=65535, nullable=true)
     */
    private $sqlsubdetail;

    /**
     * @var string
     *
     * @ORM\Column(name="colNameDetail", type="string", length=255, nullable=false)
     */
    private $colnamedetail;

    /**
     * @var string
     *
     * @ORM\Column(name="colModelDetail", type="text", length=65535, nullable=false)
     */
    private $colmodeldetail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="colNameSubDetail", type="string", length=255, nullable=true)
     */
    private $colnamesubdetail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="colModelSubDetail", type="text", length=65535, nullable=true)
     */
    private $colmodelsubdetail;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getRownum(): ?int
    {
        return $this->rownum;
    }

    public function setRownum(int $rownum): self
    {
        $this->rownum = $rownum;

        return $this;
    }

    public function getRowlist(): ?string
    {
        return $this->rowlist;
    }

    public function setRowlist(string $rowlist): self
    {
        $this->rowlist = $rowlist;

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->height;
    }

    public function setHeight(string $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getWidth(): ?string
    {
        return $this->width;
    }

    public function setWidth(string $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getSortname(): ?string
    {
        return $this->sortname;
    }

    public function setSortname(string $sortname): self
    {
        $this->sortname = $sortname;

        return $this;
    }

    public function getViewrecords(): ?string
    {
        return $this->viewrecords;
    }

    public function setViewrecords(string $viewrecords): self
    {
        $this->viewrecords = $viewrecords;

        return $this;
    }

    public function getSortorder(): ?string
    {
        return $this->sortorder;
    }

    public function setSortorder(string $sortorder): self
    {
        $this->sortorder = $sortorder;

        return $this;
    }

    public function getCaption(): ?string
    {
        return $this->caption;
    }

    public function setCaption(string $caption): self
    {
        $this->caption = $caption;

        return $this;
    }

    public function getGrouping(): ?string
    {
        return $this->grouping;
    }

    public function setGrouping(string $grouping): self
    {
        $this->grouping = $grouping;

        return $this;
    }

    public function getColModel(): ?int
    {
        return $this->colModel;
    }

    public function setColModel(int $colModel): self
    {
        $this->colModel = $colModel;

        return $this;
    }

    public function getColName(): ?int
    {
        return $this->colName;
    }

    public function setColName(int $colName): self
    {
        $this->colName = $colName;

        return $this;
    }

    public function getGroupingview(): ?string
    {
        return $this->groupingview;
    }

    public function setGroupingview(string $groupingview): self
    {
        $this->groupingview = $groupingview;

        return $this;
    }

    public function getParamdetailgrid(): ?string
    {
        return $this->paramdetailgrid;
    }

    public function setParamdetailgrid(?string $paramdetailgrid): self
    {
        $this->paramdetailgrid = $paramdetailgrid;

        return $this;
    }

    public function getParamdetailsubgrid(): ?string
    {
        return $this->paramdetailsubgrid;
    }

    public function setParamdetailsubgrid(?string $paramdetailsubgrid): self
    {
        $this->paramdetailsubgrid = $paramdetailsubgrid;

        return $this;
    }

    public function getSqldetail(): ?string
    {
        return $this->sqldetail;
    }

    public function setSqldetail(?string $sqldetail): self
    {
        $this->sqldetail = $sqldetail;

        return $this;
    }

    public function getSqlsubdetail(): ?string
    {
        return $this->sqlsubdetail;
    }

    public function setSqlsubdetail(?string $sqlsubdetail): self
    {
        $this->sqlsubdetail = $sqlsubdetail;

        return $this;
    }

    public function getColnamedetail(): ?string
    {
        return $this->colnamedetail;
    }

    public function setColnamedetail(string $colnamedetail): self
    {
        $this->colnamedetail = $colnamedetail;

        return $this;
    }

    public function getColmodeldetail(): ?string
    {
        return $this->colmodeldetail;
    }

    public function setColmodeldetail(string $colmodeldetail): self
    {
        $this->colmodeldetail = $colmodeldetail;

        return $this;
    }

    public function getColnamesubdetail(): ?string
    {
        return $this->colnamesubdetail;
    }

    public function setColnamesubdetail(?string $colnamesubdetail): self
    {
        $this->colnamesubdetail = $colnamesubdetail;

        return $this;
    }

    public function getColmodelsubdetail(): ?string
    {
        return $this->colmodelsubdetail;
    }

    public function setColmodelsubdetail(?string $colmodelsubdetail): self
    {
        $this->colmodelsubdetail = $colmodelsubdetail;

        return $this;
    }


}
