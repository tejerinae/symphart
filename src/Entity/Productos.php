<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * Productos
 *
 * @ORM\Table(name="productos")
 * @ORM\Entity
 */
class Productos
{
    /**
     * @var string
     *
     * @ORM\Column(name="codigoparticular", type="string", length=45, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $codigoparticular;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="blockd", type="boolean", nullable=true)
     */
    private $blockd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="unidad", type="string", length=5, nullable=true)
     */
    private $unidad;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo_precio", type="string", length=10, nullable=true)
     */
    private $tipoPrecio;

    /**
     * @var string
     *
     * @ORM\Column(name="rubro", type="string", length=50, nullable=false)
     */
    private $rubro;

    /**
     * @var string
     *
     * @ORM\Column(name="subrubro", type="string", length=50, nullable=false)
     */
    private $subrubro;


    /**
     * One Product has Many Prices.
     * @ORM\OneToMany(targetEntity="PreciosProducto", mappedBy="codigoparticular")
     */
    private $precios;
    
    /*public function __construct(PreciosProducto $precios)
    {
        $this->precios = new ArrayCollection(); 
    }*/

    /**
     * @return Collection|PreciosProducto[]
     */
    public function getPrecios(): Collection
    {
       /* $criteria = Criteria::create()
        ->orderBy(array('fecha' => 'DESC'))
        ->setFirstResult(0) // Empieza en 0
        ->setMaxResults(1);
        
        return $this->precios->matching($criteria);*/
        return $this->precios;
    }


    public function getCodigoparticular(): ?string
    {
        return $this->codigoparticular;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getBlockd(): ?bool
    {
        return $this->blockd;
    }

    public function setBlockd(?bool $blockd): self
    {
        $this->blockd = $blockd;

        return $this;
    }

    public function getUnidad(): ?string
    {
        return $this->unidad;
    }

    public function setUnidad(?string $unidad): self
    {
        $this->unidad = $unidad;

        return $this;
    }

    public function getTipoPrecio(): ?string
    {
        return $this->tipoPrecio;
    }

    public function setTipoPrecio(?string $tipoPrecio): self
    {
        $this->tipoPrecio = $tipoPrecio;

        return $this;
    }

    public function getRubro(): ?string
    {
        return $this->rubro;
    }

    public function setRubro(string $rubro): self
    {
        $this->rubro = $rubro;

        return $this;
    }

    public function getSubrubro(): ?string
    {
        return $this->subrubro;
    }

    public function setSubrubro(string $subrubro): self
    {
        $this->subrubro = $subrubro;

        return $this;
    }


}
