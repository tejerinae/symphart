<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Aplicaciones
 *
 * @ORM\Table(name="aplicaciones")
 * @ORM\Entity
 */
class Aplicaciones
{
    /**
     * @var int
     *
     * @ORM\Column(name="codigo", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", length=65535, nullable=false)
     */
    private $descripcion;

    public function getCodigo(): ?int
    {
        return $this->codigo;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }
}
