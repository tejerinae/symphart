<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Conjuntos
 *
 * @ORM\Table(name="conjuntos", indexes={@ORM\Index(name="conjuntos_conjuntos", columns={"id_padre"})})
 * @ORM\Entity
 */
class Conjuntos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_conjunto", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idConjunto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;
    
    /**
     * @var \Productos
     *
     * @ORM\ManyToOne(targetEntity="Productos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="codigoparticular", referencedColumnName="codigoparticular", nullable=true)
     * })
     */
    private $codigoparticular;

    /**
     * @var string
     *
     * @ORM\Column(name="cantidad", type="decimal", precision=11, scale=3, nullable=false)
     */
    private $cantidad;

    /**
     * @var \Conjuntos
     *
     * @ORM\ManyToOne(targetEntity="Conjuntos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_padre", referencedColumnName="id_conjunto")
     * })
     */
    private $idPadre;

    /**
     * @var string
     *
     * @ORM\Column(name="rubro", type="string", length=50, nullable=false)
     */
    private $rubro;
    
    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="Conjuntos", mappedBy="idPadre")
     */
    private $children;
    
    public function __construct(Conjuntos $children)
    {
        $this->children = new ArrayCollection();
    }

    /**
     * @return Collection|Conjuntos[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    private $data;
    
    public function getData()
    {
        //$this->data['id'] = $this->getId();
        $this->data['codigoparticular'] = $this->getCodigoparticular();
        if($this->data['codigoparticular']!=null){
        /*    $this->data['rubro'] = $this->data['cdigoparticular']->getRubro();
            $this->data['cdigoparticular'] = $this->getCodigoparticular()->getCodigoparticular();*/
            $this->data['tipoPrecio'] = $this->getCodigoparticular()->getTipoPrecio();
            $precio_int = $this->getCodigoparticular()->getPrecios()[0];
            $this->data['precio'] = $precio_int->getPrecio();
            $this->data['fechaPrecio'] = $precio_int->getFecha();
        }/* else {
            $this->data['rubro'] = "";
        }*/
        $this->data['idConjunto'] = $this->getIdConjunto();
        $this->data['descripcion'] = $this->getDescripcion();
        $this->data['cantidad'] = $this->getCantidad();
        $this->data['rubro'] = $this->getRubro();
        /*$this->data['codigoAplicacion'] = $this->getCodigoAplicacion();
        if($this->data['codigoAplicacion']!=null){
            $this->data['codigoAplicacion'] = $this->getCodigoAplicacion()->getCodigo();
            $this->data['aplicacion'] = $this->getCodigoAplicacion()->getDescripcion();
        } else {
            $this->data['codigoAplicacion'] = ""; 
            $this->data['aplicacion'] = "";
        }*/
        $this->data['m1'] = false;
        $this->data['m2'] = false;
        $this->data['m3'] = false;
        $this->data['m4'] = false;
        $this->data['m5'] = false;
        $this->data['m1Desc'] = '';
        $this->data['m2Desc'] = '';
        $this->data['m3Desc'] = '';
        $this->data['m4Desc'] = '';
        $this->data['m5Desc'] = '';
        $this->data['activo'] = '';
        return $this->data;
    }
    
    public function getIdConjunto(): ?int
    {
        return $this->idConjunto;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    private function getCodigoparticular(): ?Productos
    {
        return $this->codigoparticular;
    }

    /*public function setCodigoparticular(?string $codigoparticular): self
    {
        $this->codigoparticular = $codigoparticular;

        return $this;
    }*/

    private function getCantidad()
    {
        return $this->cantidad;
    }

    public function setCantidad($cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getRubro(): ?string
    {
        return $this->rubro;
    }

    public function setRubro(string $rubro): self
    {
        $this->rubro = $rubro;

        return $this;
    }
}
