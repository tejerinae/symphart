<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tchchartfunction
 *
 * @ORM\Table(name="tchchartfunction")
 * @ORM\Entity
 */
class Tchchartfunction
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="chartFunction", type="string", length=40, nullable=false)
     */
    private $chartfunction;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChartfunction(): ?string
    {
        return $this->chartfunction;
    }

    public function setChartfunction(string $chartfunction): self
    {
        $this->chartfunction = $chartfunction;

        return $this;
    }


}
