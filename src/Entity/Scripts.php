<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Scripts
 *
 * @ORM\Table(name="scripts", uniqueConstraints={@ORM\UniqueConstraint(name="nombre", columns={"nombre"})})
 * @ORM\Entity
 */
class Scripts
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="consulta", type="text", length=0, nullable=true)
     */
    private $consulta;

    /**
     * @var string
     *
     * @ORM\Column(name="otrs", type="string", length=50, nullable=false)
     */
    private $otrs;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=false)
     */
    private $nombre;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getConsulta(): ?string
    {
        return $this->consulta;
    }

    public function setConsulta(?string $consulta): self
    {
        $this->consulta = $consulta;

        return $this;
    }

    public function getOtrs(): ?string
    {
        return $this->otrs;
    }

    public function setOtrs(string $otrs): self
    {
        $this->otrs = $otrs;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }


}
