<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblChartFormater
 *
 * @ORM\Table(name="tbl_chart_formater")
 * @ORM\Entity
 */
class TblChartFormater
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_tbl_chart", type="integer", nullable=false)
     */
    private $idTblChart;

    /**
     * @var int|null
     *
     * @ORM\Column(name="formaterColumn", type="integer", nullable=true)
     */
    private $formatercolumn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="colorNegative", type="string", length=50, nullable=true, options={"default"="red"})
     */
    private $colornegative = 'red';

    /**
     * @var string|null
     *
     * @ORM\Column(name="colorPositive", type="string", length=50, nullable=true, options={"default"="blue"})
     */
    private $colorpositive = 'blue';

    /**
     * @var string
     *
     * @ORM\Column(name="showValue", type="string", length=10, nullable=false, options={"default"="true"})
     */
    private $showvalue = 'true';

    /**
     * @var int|null
     *
     * @ORM\Column(name="formaterWidth", type="integer", nullable=true)
     */
    private $formaterwidth;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdTblChart(): ?int
    {
        return $this->idTblChart;
    }

    public function setIdTblChart(int $idTblChart): self
    {
        $this->idTblChart = $idTblChart;

        return $this;
    }

    public function getFormatercolumn(): ?int
    {
        return $this->formatercolumn;
    }

    public function setFormatercolumn(?int $formatercolumn): self
    {
        $this->formatercolumn = $formatercolumn;

        return $this;
    }

    public function getColornegative(): ?string
    {
        return $this->colornegative;
    }

    public function setColornegative(?string $colornegative): self
    {
        $this->colornegative = $colornegative;

        return $this;
    }

    public function getColorpositive(): ?string
    {
        return $this->colorpositive;
    }

    public function setColorpositive(?string $colorpositive): self
    {
        $this->colorpositive = $colorpositive;

        return $this;
    }

    public function getShowvalue(): ?string
    {
        return $this->showvalue;
    }

    public function setShowvalue(string $showvalue): self
    {
        $this->showvalue = $showvalue;

        return $this;
    }

    public function getFormaterwidth(): ?int
    {
        return $this->formaterwidth;
    }

    public function setFormaterwidth(?int $formaterwidth): self
    {
        $this->formaterwidth = $formaterwidth;

        return $this;
    }


}
