<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblChart
 *
 * @ORM\Table(name="tbl_chart")
 * @ORM\Entity
 */
class TblChart
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="allowHtml", type="string", length=10, nullable=true)
     */
    private $allowhtml;

    /**
     * @var string|null
     *
     * @ORM\Column(name="alternatingRowStyle", type="string", length=255, nullable=true)
     */
    private $alternatingrowstyle;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cssClassNames", type="integer", nullable=true)
     */
    private $cssclassnames;

    /**
     * @var string|null
     *
     * @ORM\Column(name="height", type="string", length=150, nullable=true)
     */
    private $height;

    /**
     * @var string|null
     *
     * @ORM\Column(name="width", type="string", length=150, nullable=true)
     */
    private $width;

    /**
     * @var string|null
     *
     * @ORM\Column(name="page", type="string", length=50, nullable=true)
     */
    private $page;

    /**
     * @var int|null
     *
     * @ORM\Column(name="pageSize", type="integer", nullable=true)
     */
    private $pagesize;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sort", type="string", length=10, nullable=true)
     */
    private $sort;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sortColumn", type="string", length=50, nullable=true)
     */
    private $sortcolumn;

    /**
     * @var int|null
     *
     * @ORM\Column(name="formaterColumn", type="integer", nullable=true)
     */
    private $formatercolumn;

    /**
     * @var int
     *
     * @ORM\Column(name="formaterWidth", type="integer", nullable=false)
     */
    private $formaterwidth;

    /**
     * @var string
     *
     * @ORM\Column(name="stringFilter", type="string", length=10, nullable=false, options={"default"="false"})
     */
    private $stringfilter = 'false';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getAllowhtml(): ?string
    {
        return $this->allowhtml;
    }

    public function setAllowhtml(?string $allowhtml): self
    {
        $this->allowhtml = $allowhtml;

        return $this;
    }

    public function getAlternatingrowstyle(): ?string
    {
        return $this->alternatingrowstyle;
    }

    public function setAlternatingrowstyle(?string $alternatingrowstyle): self
    {
        $this->alternatingrowstyle = $alternatingrowstyle;

        return $this;
    }

    public function getCssclassnames(): ?int
    {
        return $this->cssclassnames;
    }

    public function setCssclassnames(?int $cssclassnames): self
    {
        $this->cssclassnames = $cssclassnames;

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->height;
    }

    public function setHeight(?string $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getWidth(): ?string
    {
        return $this->width;
    }

    public function setWidth(?string $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getPage(): ?string
    {
        return $this->page;
    }

    public function setPage(?string $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getPagesize(): ?int
    {
        return $this->pagesize;
    }

    public function setPagesize(?int $pagesize): self
    {
        $this->pagesize = $pagesize;

        return $this;
    }

    public function getSort(): ?string
    {
        return $this->sort;
    }

    public function setSort(?string $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    public function getSortcolumn(): ?string
    {
        return $this->sortcolumn;
    }

    public function setSortcolumn(?string $sortcolumn): self
    {
        $this->sortcolumn = $sortcolumn;

        return $this;
    }

    public function getFormatercolumn(): ?int
    {
        return $this->formatercolumn;
    }

    public function setFormatercolumn(?int $formatercolumn): self
    {
        $this->formatercolumn = $formatercolumn;

        return $this;
    }

    public function getFormaterwidth(): ?int
    {
        return $this->formaterwidth;
    }

    public function setFormaterwidth(int $formaterwidth): self
    {
        $this->formaterwidth = $formaterwidth;

        return $this;
    }

    public function getStringfilter(): ?string
    {
        return $this->stringfilter;
    }

    public function setStringfilter(string $stringfilter): self
    {
        $this->stringfilter = $stringfilter;

        return $this;
    }


}
