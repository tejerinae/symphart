<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Drilldown
 *
 * @ORM\Table(name="drilldown")
 * @ORM\Entity
 */
class Drilldown
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tabchart_id", type="integer", nullable=true)
     */
    private $tabchartId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tabchart_id_link", type="integer", nullable=true)
     */
    private $tabchartIdLink;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTabchartId(): ?int
    {
        return $this->tabchartId;
    }

    public function setTabchartId(?int $tabchartId): self
    {
        $this->tabchartId = $tabchartId;

        return $this;
    }

    public function getTabchartIdLink(): ?int
    {
        return $this->tabchartIdLink;
    }

    public function setTabchartIdLink(?int $tabchartIdLink): self
    {
        $this->tabchartIdLink = $tabchartIdLink;

        return $this;
    }


}
