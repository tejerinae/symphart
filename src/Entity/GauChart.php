<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GauChart
 *
 * @ORM\Table(name="gau_chart", uniqueConstraints={@ORM\UniqueConstraint(name="nombre", columns={"nombre"})})
 * @ORM\Entity
 */
class GauChart
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tpo", type="string", length=50, nullable=true)
     */
    private $tpo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=55, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="height", type="string", length=255, nullable=true)
     */
    private $height;

    /**
     * @var string|null
     *
     * @ORM\Column(name="width", type="string", length=20, nullable=true)
     */
    private $width;

    /**
     * @var int|null
     *
     * @ORM\Column(name="animation_duration", type="integer", nullable=true)
     */
    private $animationDuration;

    /**
     * @var string|null
     *
     * @ORM\Column(name="animation_easing", type="string", length=50, nullable=true)
     */
    private $animationEasing;

    /**
     * @var string|null
     *
     * @ORM\Column(name="greenColor", type="string", length=50, nullable=true)
     */
    private $greencolor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="greenFrom", type="string", length=50, nullable=true)
     */
    private $greenfrom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="greenTo", type="string", length=50, nullable=true)
     */
    private $greento;

    /**
     * @var int|null
     *
     * @ORM\Column(name="max", type="integer", nullable=true)
     */
    private $max;

    /**
     * @var int|null
     *
     * @ORM\Column(name="min", type="integer", nullable=true)
     */
    private $min;

    /**
     * @var string|null
     *
     * @ORM\Column(name="redColor", type="string", length=50, nullable=true)
     */
    private $redcolor;

    /**
     * @var int|null
     *
     * @ORM\Column(name="redFrom", type="integer", nullable=true)
     */
    private $redfrom;

    /**
     * @var int|null
     *
     * @ORM\Column(name="redTo", type="integer", nullable=true)
     */
    private $redto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="yellowColor", type="string", length=50, nullable=true)
     */
    private $yellowcolor;

    /**
     * @var int|null
     *
     * @ORM\Column(name="yellowFrom", type="integer", nullable=true)
     */
    private $yellowfrom;

    /**
     * @var int|null
     *
     * @ORM\Column(name="yellowTo", type="integer", nullable=true)
     */
    private $yellowto;

    /**
     * @var int|null
     *
     * @ORM\Column(name="minorTicks", type="integer", nullable=true)
     */
    private $minorticks;

    /**
     * @var string|null
     *
     * @ORM\Column(name="majorTicks", type="string", length=255, nullable=true)
     */
    private $majorticks;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTpo(): ?string
    {
        return $this->tpo;
    }

    public function setTpo(?string $tpo): self
    {
        $this->tpo = $tpo;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->height;
    }

    public function setHeight(?string $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getWidth(): ?string
    {
        return $this->width;
    }

    public function setWidth(?string $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getAnimationDuration(): ?int
    {
        return $this->animationDuration;
    }

    public function setAnimationDuration(?int $animationDuration): self
    {
        $this->animationDuration = $animationDuration;

        return $this;
    }

    public function getAnimationEasing(): ?string
    {
        return $this->animationEasing;
    }

    public function setAnimationEasing(?string $animationEasing): self
    {
        $this->animationEasing = $animationEasing;

        return $this;
    }

    public function getGreencolor(): ?string
    {
        return $this->greencolor;
    }

    public function setGreencolor(?string $greencolor): self
    {
        $this->greencolor = $greencolor;

        return $this;
    }

    public function getGreenfrom(): ?string
    {
        return $this->greenfrom;
    }

    public function setGreenfrom(?string $greenfrom): self
    {
        $this->greenfrom = $greenfrom;

        return $this;
    }

    public function getGreento(): ?string
    {
        return $this->greento;
    }

    public function setGreento(?string $greento): self
    {
        $this->greento = $greento;

        return $this;
    }

    public function getMax(): ?int
    {
        return $this->max;
    }

    public function setMax(?int $max): self
    {
        $this->max = $max;

        return $this;
    }

    public function getMin(): ?int
    {
        return $this->min;
    }

    public function setMin(?int $min): self
    {
        $this->min = $min;

        return $this;
    }

    public function getRedcolor(): ?string
    {
        return $this->redcolor;
    }

    public function setRedcolor(?string $redcolor): self
    {
        $this->redcolor = $redcolor;

        return $this;
    }

    public function getRedfrom(): ?int
    {
        return $this->redfrom;
    }

    public function setRedfrom(?int $redfrom): self
    {
        $this->redfrom = $redfrom;

        return $this;
    }

    public function getRedto(): ?int
    {
        return $this->redto;
    }

    public function setRedto(?int $redto): self
    {
        $this->redto = $redto;

        return $this;
    }

    public function getYellowcolor(): ?string
    {
        return $this->yellowcolor;
    }

    public function setYellowcolor(?string $yellowcolor): self
    {
        $this->yellowcolor = $yellowcolor;

        return $this;
    }

    public function getYellowfrom(): ?int
    {
        return $this->yellowfrom;
    }

    public function setYellowfrom(?int $yellowfrom): self
    {
        $this->yellowfrom = $yellowfrom;

        return $this;
    }

    public function getYellowto(): ?int
    {
        return $this->yellowto;
    }

    public function setYellowto(?int $yellowto): self
    {
        $this->yellowto = $yellowto;

        return $this;
    }

    public function getMinorticks(): ?int
    {
        return $this->minorticks;
    }

    public function setMinorticks(?int $minorticks): self
    {
        $this->minorticks = $minorticks;

        return $this;
    }

    public function getMajorticks(): ?string
    {
        return $this->majorticks;
    }

    public function setMajorticks(?string $majorticks): self
    {
        $this->majorticks = $majorticks;

        return $this;
    }


}
