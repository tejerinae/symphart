<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Clientes
 *
 * @ORM\Table(name="clientes", uniqueConstraints={@ORM\UniqueConstraint(name="codigoparticular", columns={"codigoparticular"}), @ORM\UniqueConstraint(name="codigocliente", columns={"codigocliente"})})
 * @ORM\Entity(repositoryClass="App\Repository\ClientesRepository")
 */
class Clientes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cuit", type="string", length=45, nullable=true)
     */
    private $cuit;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigocliente", type="string", length=45, nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $codigocliente;

    /**
     * @var string
     *
     * @ORM\Column(name="codigoparticular", type="string", length=45, nullable=false)
     */
    private $codigoparticular;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getCuit(): ?string
    {
        return $this->cuit;
    }

    public function setCuit(?string $cuit): self
    {
        $this->cuit = $cuit;

        return $this;
    }

    public function getCodigocliente(): ?string
    {
        return $this->codigocliente;
    }
    
    public function setCodigocliente(string $codigocliente): self
    {
        $this->codigocliente = $codigocliente;

        return $this;
    }
    public function getCodigoparticular(): ?string
    {
        return $this->codigoparticular;
    }

    public function setCodigoparticular(string $codigoparticular): self
    {
        $this->codigoparticular = $codigoparticular;

        return $this;
    }


}
