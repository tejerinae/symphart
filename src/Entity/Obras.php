<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Obras
 *
 * @ORM\Table(name="obras", uniqueConstraints={@ORM\UniqueConstraint(name="codigoproyecto", columns={"codigoproyecto"})}, indexes={@ORM\Index(name="clientes_obras", columns={"codigocliente"})})
 * @ORM\Entity
 */
class Obras
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=45, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigoproyecto", type="string", length=45, nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $codigoproyecto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigoresponsableproyecto", type="string", length=45, nullable=true)
     */
    private $codigoresponsableproyecto;

    /**
     * @var bool
     *
     * @ORM\Column(name="finalizado", type="boolean", nullable=false)
     */
    private $finalizado = '0';

    /**
     * @var \Clientes
     *
     * @ORM\ManyToOne(targetEntity="Clientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="codigocliente", referencedColumnName="codigocliente")
     * })
     */
    private $codigocliente;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getCodigoproyecto(): ?string
    {
        return $this->codigoproyecto;
    }

    public function setCodigoproyecto(?string $codigoproyecto): self
    {
        $this->codigoproyecto = $codigoproyecto;

        return $this;
    }

    public function getCodigoresponsableproyecto(): ?string
    {
        return $this->codigoresponsableproyecto;
    }

    public function setCodigoresponsableproyecto(?string $codigoresponsableproyecto): self
    {
        $this->codigoresponsableproyecto = $codigoresponsableproyecto;

        return $this;
    }

    public function getFinalizado(): ?bool
    {
        return $this->finalizado;
    }

    public function setFinalizado(bool $finalizado): self
    {
        $this->finalizado = $finalizado;

        return $this;
    }

    public function getCodigocliente(): ?Clientes
    {
        return $this->codigocliente;
    }

    public function setCodigocliente(?Clientes $codigocliente): self
    {
        $this->codigocliente = $codigocliente;

        return $this;
    }


}
