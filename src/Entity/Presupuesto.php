<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Presupuesto
 *
 * @ORM\Table(name="presupuesto", indexes={@ORM\Index(name="idx1", columns={"cliente", "obra"})})
 * @ORM\Entity(repositoryClass="App\Repository\PresupuestoRepository")
 */
class Presupuesto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cliente", type="string", length=150, nullable=true)
     */
    private $cliente;

    /**
     * @var string|null
     *
     * @ORM\Column(name="obra", type="string", length=45, nullable=true)
     */
    private $obra;

    /**
     * @var string|null
     *
     * @ORM\Column(name="version", type="string", length=15, nullable=true)
     */
    private $version;

    /**
     * @var float
     *
     * @ORM\Column(name="m1", type="float", precision=5, scale=2, nullable=false, options={"default"="0.00"})
     */
    private $m1 = '0.00';

    /**
     * @var float
     *
     * @ORM\Column(name="m2", type="float", precision=5, scale=2, nullable=false, options={"default"="0.00"})
     */
    private $m2 = '0.00';

    /**
     * @var float
     *
     * @ORM\Column(name="m3", type="float", precision=5, scale=2, nullable=false, options={"default"="0.00"})
     */
    private $m3 = '0.00';

    /**
     * @var float
     *
     * @ORM\Column(name="m4", type="float", precision=5, scale=2, nullable=false, options={"default"="0.00"})
     */
    private $m4 = '0.00';

    /**
     * @var float
     *
     * @ORM\Column(name="m5", type="float", precision=5, scale=2, nullable=false, options={"default"="0.00"})
     */
    private $m5 = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="m1_desc", type="string", length=255, nullable=false, options={"default"="Modificador de precio"})
     */
    private $m1Desc = 'Modificador de precio';

    /**
     * @var string
     *
     * @ORM\Column(name="m2_desc", type="string", length=255, nullable=false, options={"default"="Modificador de precio"})
     */
    private $m2Desc = 'Modificador de precio';

    /**
     * @var string
     *
     * @ORM\Column(name="m3_desc", type="string", length=255, nullable=false, options={"default"="Modificador de precio"})
     */
    private $m3Desc = 'Modificador de precio';

    /**
     * @var string
     *
     * @ORM\Column(name="m4_desc", type="string", length=255, nullable=false, options={"default"="Modificador de precio"})
     */
    private $m4Desc = 'Modificador de precio';

    /**
     * @var string
     *
     * @ORM\Column(name="m5_desc", type="string", length=255, nullable=false, options={"default"="Modificador de precio"})
     */
    private $m5Desc = 'Modificador de precio';

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="ItemsTrans", mappedBy="idPresupuesto")
     */
    private $items;
    
    /**
     * @return Collection|Product[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }
    
    public function setItems($items): self
    {
        $this->items = $items;

        return $this;
    }
    /**
     * Many presupuertos have One Clientes.
     * @ORM\ManyToOne(targetEntity="Clientes")
     * @ORM\JoinColumn(name="cliente", referencedColumnName="codigocliente")
     */
    private $nombre;
    
    /**
     * Many resupuertos have One Obra.
     * @ORM\ManyToOne(targetEntity="Obras")
     * @ORM\JoinColumn(name="obra", referencedColumnName="codigoproyecto")
     */
    private $nombre_obra;

    public function getCliente(): ?Clientes
    {
        return $this->nombre;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setCliente(?Clientes $cliente): self
    {   
        $this->nombre = $cliente;
        return $this;
    }

    public function getObra(): ?Obras
    {
        return $this->nombre_obra;
    }

    public function setObra(?Obras $obra): self
    {
        $this->nombre_obra = $obra;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(?string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getM1()
    {
        return $this->m1;
    }

    public function setM1($m1): self
    {
        $this->m1 = $m1;

        return $this;
    }

    public function getM2()
    {
        return $this->m2;
    }

    public function setM2($m2): self
    {
        $this->m2 = $m2;

        return $this;
    }

    public function getM3()
    {
        return $this->m3;
    }

    public function setM3($m3): self
    {
        $this->m3 = $m3;

        return $this;
    }

    public function getM4()
    {
        return $this->m4;
    }

    public function setM4($m4): self
    {
        $this->m4 = $m4;

        return $this;
    }

    public function getM5()
    {
        return $this->m5;
    }

    public function setM5($m5): self
    {
        $this->m5 = $m5;

        return $this;
    }

    public function getM1Desc(): ?string
    {
        return $this->m1Desc;
    }

    public function setM1Desc(string $m1Desc): self
    {
        $this->m1Desc = $m1Desc;

        return $this;
    }

    public function getM2Desc(): ?string
    {
        return $this->m2Desc;
    }

    public function setM2Desc(string $m2Desc): self
    {
        $this->m2Desc = $m2Desc;

        return $this;
    }

    public function getM3Desc(): ?string
    {
        return $this->m3Desc;
    }

    public function setM3Desc(string $m3Desc): self
    {
        $this->m3Desc = $m3Desc;

        return $this;
    }

    public function getM4Desc(): ?string
    {
        return $this->m4Desc;
    }

    public function setM4Desc(string $m4Desc): self
    {
        $this->m4Desc = $m4Desc;

        return $this;
    }

    public function getM5Desc(): ?string
    {
        return $this->m5Desc;
    }

    public function setM5Desc(string $m5Desc): self
    {
        $this->m5Desc = $m5Desc;

        return $this;
    }

   
}
