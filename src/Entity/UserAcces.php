<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserAcces
 *
 * @ORM\Table(name="user_acces")
 * @ORM\Entity
 */
class UserAcces
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="user", type="string", length=50, nullable=false)
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\Column(name="id_permission", type="integer", nullable=false)
     */
    private $idPermission;

    /**
     * @var string|null
     *
     * @ORM\Column(name="obs", type="string", length=200, nullable=true)
     */
    private $obs;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?string
    {
        return $this->user;
    }

    public function setUser(string $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIdPermission(): ?int
    {
        return $this->idPermission;
    }

    public function setIdPermission(int $idPermission): self
    {
        $this->idPermission = $idPermission;

        return $this;
    }

    public function getObs(): ?string
    {
        return $this->obs;
    }

    public function setObs(?string $obs): self
    {
        $this->obs = $obs;

        return $this;
    }


}
