<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblStringFilter
 *
 * @ORM\Table(name="tbl_string_filter")
 * @ORM\Entity
 */
class TblStringFilter
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_tbl_chart", type="integer", nullable=false)
     */
    private $idTblChart;

    /**
     * @var string
     *
     * @ORM\Column(name="filterColumnLabel", type="string", length=100, nullable=false)
     */
    private $filtercolumnlabel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="label", type="string", length=100, nullable=true, options={"default"="LabelFilter"})
     */
    private $label = 'LabelFilter';

    /**
     * @var string|null
     *
     * @ORM\Column(name="labelSeparator", type="string", length=5, nullable=true, options={"default"=":"})
     */
    private $labelseparator = ':';

    /**
     * @var string|null
     *
     * @ORM\Column(name="labelStacking", type="string", length=11, nullable=true, options={"default"="horizontal"})
     */
    private $labelstacking = 'horizontal';

    /**
     * @var string
     *
     * @ORM\Column(name="filterType", type="string", length=25, nullable=false, options={"default"="StringFilter"})
     */
    private $filtertype = 'StringFilter';

    /**
     * @var string
     *
     * @ORM\Column(name="allowTyping", type="string", length=10, nullable=false, options={"default"="false"})
     */
    private $allowtyping = 'false';

    /**
     * @var string
     *
     * @ORM\Column(name="allowMultiple", type="string", length=15, nullable=false, options={"default"="false"})
     */
    private $allowmultiple = 'false';

    /**
     * @var string
     *
     * @ORM\Column(name="selectedValuesLayout", type="string", length=25, nullable=false, options={"default"="belowStacked"})
     */
    private $selectedvalueslayout = 'belowStacked';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdTblChart(): ?int
    {
        return $this->idTblChart;
    }

    public function setIdTblChart(int $idTblChart): self
    {
        $this->idTblChart = $idTblChart;

        return $this;
    }

    public function getFiltercolumnlabel(): ?string
    {
        return $this->filtercolumnlabel;
    }

    public function setFiltercolumnlabel(string $filtercolumnlabel): self
    {
        $this->filtercolumnlabel = $filtercolumnlabel;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getLabelseparator(): ?string
    {
        return $this->labelseparator;
    }

    public function setLabelseparator(?string $labelseparator): self
    {
        $this->labelseparator = $labelseparator;

        return $this;
    }

    public function getLabelstacking(): ?string
    {
        return $this->labelstacking;
    }

    public function setLabelstacking(?string $labelstacking): self
    {
        $this->labelstacking = $labelstacking;

        return $this;
    }

    public function getFiltertype(): ?string
    {
        return $this->filtertype;
    }

    public function setFiltertype(string $filtertype): self
    {
        $this->filtertype = $filtertype;

        return $this;
    }

    public function getAllowtyping(): ?string
    {
        return $this->allowtyping;
    }

    public function setAllowtyping(string $allowtyping): self
    {
        $this->allowtyping = $allowtyping;

        return $this;
    }

    public function getAllowmultiple(): ?string
    {
        return $this->allowmultiple;
    }

    public function setAllowmultiple(string $allowmultiple): self
    {
        $this->allowmultiple = $allowmultiple;

        return $this;
    }

    public function getSelectedvalueslayout(): ?string
    {
        return $this->selectedvalueslayout;
    }

    public function setSelectedvalueslayout(string $selectedvalueslayout): self
    {
        $this->selectedvalueslayout = $selectedvalueslayout;

        return $this;
    }


}
