<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GridColName
 *
 * @ORM\Table(name="grid_col_name")
 * @ORM\Entity
 */
class GridColName
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="col_names", type="string", length=255, nullable=false)
     */
    private $colNames;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getColNames(): ?string
    {
        return $this->colNames;
    }

    public function setColNames(string $colNames): self
    {
        $this->colNames = $colNames;

        return $this;
    }


}
