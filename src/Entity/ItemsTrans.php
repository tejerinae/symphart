<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ItemsTrans
 *
 * @ORM\Table(name="items_trans", indexes={@ORM\Index(name="FK_items_trans_presupuesto", columns={"id_presupuesto"}), @ORM\Index(name="FK_items_trans_productos", columns={"codigoparticular"}), @ORM\Index(name="FK_items_trans_aplicaciones", columns={"codigo_aplicacion"}), @ORM\Index(name="FK_items_trans_items_trans", columns={"id_padre"})})
 * @ORM\Entity(repositoryClass="App\Repository\ItemsTransRepository")
 */
class ItemsTrans
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_conjunto", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $idConjunto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo_precio", type="string", length=10, nullable=true)
     */
    private $tipoPrecio;

     /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_precio", type="date", nullable=false)
     */
    private $fechaPrecio;

    /**
     * @var string
     *
     * @ORM\Column(name="cantidad", type="decimal", precision=11, scale=3, nullable=false, options={"default"="1.000"})
     */
    private $cantidad = '1.000';

    /**
     * @var string|null
     *
     * @ORM\Column(name="precio", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precio;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="m1", type="boolean", nullable=true)
     */
    private $m1 = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="m2", type="boolean", nullable=true)
     */
    private $m2 = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="m3", type="boolean", nullable=true)
     */
    private $m3 = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="m4", type="boolean", nullable=true)
     */
    private $m4 = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="m5", type="boolean", nullable=true)
     */
    private $m5 = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="m1_desc", type="string", length=255, nullable=false)
     */
    private $m1Desc;

    /**
     * @var string
     *
     * @ORM\Column(name="m2_desc", type="string", length=255, nullable=false)
     */
    private $m2Desc;

    /**
     * @var string
     *
     * @ORM\Column(name="m3_desc", type="string", length=255, nullable=false)
     */
    private $m3Desc;

    /**
     * @var string
     *
     * @ORM\Column(name="m4_desc", type="string", length=255, nullable=false)
     */
    private $m4Desc;

    /**
     * @var string
     *
     * @ORM\Column(name="m5_desc", type="string", length=255, nullable=false)
     */
    private $m5Desc;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="activo", type="boolean", nullable=true, options={"default"="1"})
     */
    private $activo = '1';

    /**
     * @var \Aplicaciones
     *
     * @ORM\ManyToOne(targetEntity="Aplicaciones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="codigo_aplicacion", referencedColumnName="codigo")
     * })
     */
    private $codigoAplicacion;

    /**
     * @var \ItemsTrans
     *
     * @ORM\ManyToOne(targetEntity="ItemsTrans", inversedBy="itemsTrans", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_padre", referencedColumnName="id")
     * })
     */
    private $idPadre;

    /**
     * @var \Presupuesto
     *
     * @ORM\ManyToOne(targetEntity="Presupuesto", inversedBy="itemsTrans", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_presupuesto", referencedColumnName="id")
     * })
     */
    private $idPresupuesto;

    /**
     * @var \Productos
     *
     * @ORM\ManyToOne(targetEntity="Productos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="codigoparticular", referencedColumnName="codigoparticular")
     * })
     */
    private $codigoparticular;


    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="ItemsTrans", mappedBy="idPadre")
     */
    private $children;
    
    /*public function __construct(ItemsTrans $children)
    {
        $this->children = new ArrayCollection();
    }*/

    /**
     * @return Collection|ItemsTrans[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    private $data;
    
    public function getData()
    {
        $this->data['id'] = $this->getId();
        $this->data['codigoparticular'] = $this->getCodigoparticular();
        /*if($this->data['cdigoparticular']!=null){
            $this->data['cdigoparticular']['rubro'] = $this->data['cdigoparticular']->getRubro();
            $this->data['cdigoparticular']['cdigoparticular'] = $this->getCodigoparticular()->getCodigoparticular();
        } else {
            $this->data['rubro'] = "";
        }*/
        $this->data['idConjunto'] = $this->getIdConjunto();
        $this->data['descripcion'] = $this->getDescripcion();
        $this->data['fechaPrecio'] = $this->getFechaPrecio();
        $this->data['cantidad'] = $this->getCantidad();
        $this->data['precio'] = $this->getPrecio();
        $this->data['tipoPrecio'] = $this->getTipoPrecio();
        $this->data['codigoAplicacion'] = $this->getCodigoAplicacion();
        if($this->data['codigoAplicacion']!=null){
            $this->data['codigoAplicacion'] = $this->getCodigoAplicacion()->getCodigo();
            $this->data['aplicacion'] = $this->getCodigoAplicacion()->getDescripcion();
        } else {
            $this->data['codigoAplicacion'] = ""; 
            $this->data['aplicacion'] = "";
        }
        $this->data['m1'] = $this->getM1();
        $this->data['m2'] = $this->getM2();
        $this->data['m3'] = $this->getM3();
        $this->data['m4'] = $this->getM4();
        $this->data['m5'] = $this->getM5();
        $this->data['m1Desc'] = $this->getM1Desc();
        $this->data['m2Desc'] = $this->getM2Desc();
        $this->data['m3Desc'] = $this->getM3Desc();
        $this->data['m4Desc'] = $this->getM4Desc();
        $this->data['m5Desc'] = $this->getM5Desc();
        $this->data['activo'] = $this->getActivo();
        return $this->data;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    
    /*public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }*/

    /*public function getIdPresupuesto(): ?int
    {
        return $this->idPresupuesto;
    }*/

    public function setIdPresupuesto(?Presupuesto $idPresupuesto): self
    {
        $this->idPresupuesto = $idPresupuesto;

        return $this;
    }
    
    private function getCodigoparticular(): ?Productos
    {
        return $this->codigoparticular;
    }

    public function setCodigoparticular(?Productos $codigoparticular): self
    {
        $this->codigoparticular = $codigoparticular;

        return $this;
    }

   /* public function getIdPadre(): ?ItemsTrans
    {
        return $this->idPadre;
    }*/

    public function setIdPadre(?ItemsTrans $idPadre): self
    {
        $this->idPadre = $idPadre;

        return $this;
    }

    private function getFechaPrecio(): ?string
    {
        return $this->fechaPrecio->format('d/m/y');
    }

    public function setFechaPrecio(\DateTime $fechaPrecio)
    {
        $this->fechaPrecio = $fechaPrecio;
    }
    //public function setFechaPrecio(?string $fechaPrecio): self
    //{
        
        //$time = strtotime($fechaPrecio);
        //$newformat = date('Y-m-d',$time);
        //$this->fechaPrecio = $newformat;

        //return $this;
    //}

    private function getIdConjunto(): ?int
    {
        return $this->idConjunto;
    }

    public function setIdConjunto(?int $idConjunto): self
    {
        $this->idConjunto = $idConjunto;

        return $this;
    }

    private function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    private function getTipoPrecio(): ?string
    {
        return $this->tipoPrecio;
    }

    public function setTipoPrecio(?string $tipoPrecio): self
    {
        $this->tipoPrecio = $tipoPrecio;
      
        return $this;
    }

    private function getCantidad()
    {
        return $this->cantidad;
    }

    public function setCantidad($cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    private function getPrecio()
    {
        return $this->precio;
    }

    public function setPrecio($precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    private function getCodigoAplicacion(): ?Aplicaciones
    {
        return $this->codigoAplicacion;
    }

    public function setCodigoAplicacion(?Aplicaciones $codigoAplicacion): self
    {
        $this->codigoAplicacion = $codigoAplicacion;

        return $this;
    }

    private function getM1(): ?bool
    {
        return $this->m1;
    }

    public function setM1(?bool $m1): self
    {
        $this->m1 = $m1;

        return $this;
    }

    private function getM2(): ?bool
    {
        return $this->m2;
    }

    public function setM2(?bool $m2): self
    {
        $this->m2 = $m2;

        return $this;
    }

    private function getM3(): ?bool
    {
        return $this->m3;
    }

    public function setM3(?bool $m3): self
    {
        $this->m3 = $m3;

        return $this;
    }

    private function getM4(): ?bool
    {
        return $this->m4;
    }

    public function setM4(?bool $m4): self
    {
        $this->m4 = $m4;

        return $this;
    }

    private function getM5(): ?bool
    {
        return $this->m5;
    }

    public function setM5(?bool $m5): self
    {
        $this->m5 = $m5;

        return $this;
    }

    private function getM1Desc(): ?string
    {
        return $this->m1Desc;
    }

    public function setM1Desc(string $m1Desc): self
    {
        $this->m1Desc = $m1Desc;

        return $this;
    }

    private function getM2Desc(): ?string
    {
        return $this->m2Desc;
    }

    public function setM2Desc(string $m2Desc): self
    {
        $this->m2Desc = $m2Desc;

        return $this;
    }

    private function getM3Desc(): ?string
    {
        return $this->m3Desc;
    }

    public function setM3Desc(string $m3Desc): self
    {
        $this->m3Desc = $m3Desc;

        return $this;
    }

    private function getM4Desc(): ?string
    {
        return $this->m4Desc;
    }

    public function setM4Desc(string $m4Desc): self
    {
        $this->m4Desc = $m4Desc;

        return $this;
    }

    private function getM5Desc(): ?string
    {
        return $this->m5Desc;
    }

    public function setM5Desc(string $m5Desc): self
    {
        $this->m5Desc = $m5Desc;

        return $this;
    }

    private function getActivo(): ?bool
    {
        return $this->activo;
    }

    public function setActivo(?bool $activo): self
    {
        $this->activo = $activo;

        return $this;
    }

   
    
    /**
     * Many ItemsTrans have One Productos.
     * @ORM\ManyToOne(targetEntity="Productos")
     * @ORM\JoinColumn(name="codigoparticular", referencedColumnName="codigoparticular",nullable=true)
     */
    /*private $producto;

    public function getArticulo(): ?Clientes
    {
        return $this->producto;
    }*/
}
