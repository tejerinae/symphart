<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tabs
 *
 * @ORM\Table(name="tabs")
 * @ORM\Entity
 */
class Tabs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="template", type="string", length=200, nullable=false)
     */
    private $template;

    /**
     * @var int
     *
     * @ORM\Column(name="id_link", type="integer", nullable=false)
     */
    private $idLink;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=150, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=250, nullable=false)
     */
    private $titulo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTemplate(): ?string
    {
        return $this->template;
    }

    public function setTemplate(string $template): self
    {
        $this->template = $template;

        return $this;
    }

    public function getIdLink(): ?int
    {
        return $this->idLink;
    }

    public function setIdLink(int $idLink): self
    {
        $this->idLink = $idLink;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }


}
