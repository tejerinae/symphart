<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PieChart
 *
 * @ORM\Table(name="pie_chart", uniqueConstraints={@ORM\UniqueConstraint(name="nombre", columns={"nombre"})})
 * @ORM\Entity
 */
class PieChart
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tpo", type="string", length=50, nullable=true)
     */
    private $tpo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=55, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="backgroundColor", type="string", length=255, nullable=true)
     */
    private $backgroundcolor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="chartArea", type="string", length=255, nullable=true)
     */
    private $chartarea;

    /**
     * @var string|null
     *
     * @ORM\Column(name="colors", type="string", length=255, nullable=true)
     */
    private $colors;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fontSize", type="string", length=255, nullable=true)
     */
    private $fontsize;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fontName", type="string", length=255, nullable=true)
     */
    private $fontname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="height", type="string", length=255, nullable=true)
     */
    private $height;

    /**
     * @var string|null
     *
     * @ORM\Column(name="legend", type="string", length=255, nullable=true)
     */
    private $legend;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titlePosition", type="string", length=255, nullable=true)
     */
    private $titleposition;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titleTextStyle", type="string", length=255, nullable=true)
     */
    private $titletextstyle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="width", type="string", length=20, nullable=true)
     */
    private $width;

    /**
     * @var string|null
     *
     * @ORM\Column(name="is3D", type="string", length=20, nullable=true)
     */
    private $is3d;

    /**
     * @var string|null
     *
     * @ORM\Column(name="slices", type="string", length=255, nullable=true)
     */
    private $slices;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pieSliceBorderColor", type="string", length=50, nullable=true)
     */
    private $pieslicebordercolor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sliceVisibilityThreshold", type="string", length=20, nullable=true)
     */
    private $slicevisibilitythreshold;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pieResidueSliceLabel", type="string", length=200, nullable=true)
     */
    private $pieresidueslicelabel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="backgroundColor_stroke", type="string", length=20, nullable=true)
     */
    private $backgroundcolorStroke;

    /**
     * @var int|null
     *
     * @ORM\Column(name="backgroundColor_strokeWidth", type="integer", nullable=true)
     */
    private $backgroundcolorStrokewidth;

    /**
     * @var string|null
     *
     * @ORM\Column(name="legend_textStyle", type="string", length=245, nullable=true)
     */
    private $legendTextstyle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="reverseCategories", type="string", length=20, nullable=true)
     */
    private $reversecategories;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tooltip_showColorCode", type="string", length=10, nullable=true)
     */
    private $tooltipShowcolorcode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tooltip_textStyle", type="string", length=250, nullable=true)
     */
    private $tooltipTextstyle;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTpo(): ?string
    {
        return $this->tpo;
    }

    public function setTpo(?string $tpo): self
    {
        $this->tpo = $tpo;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getBackgroundcolor(): ?string
    {
        return $this->backgroundcolor;
    }

    public function setBackgroundcolor(?string $backgroundcolor): self
    {
        $this->backgroundcolor = $backgroundcolor;

        return $this;
    }

    public function getChartarea(): ?string
    {
        return $this->chartarea;
    }

    public function setChartarea(?string $chartarea): self
    {
        $this->chartarea = $chartarea;

        return $this;
    }

    public function getColors(): ?string
    {
        return $this->colors;
    }

    public function setColors(?string $colors): self
    {
        $this->colors = $colors;

        return $this;
    }

    public function getFontsize(): ?string
    {
        return $this->fontsize;
    }

    public function setFontsize(?string $fontsize): self
    {
        $this->fontsize = $fontsize;

        return $this;
    }

    public function getFontname(): ?string
    {
        return $this->fontname;
    }

    public function setFontname(?string $fontname): self
    {
        $this->fontname = $fontname;

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->height;
    }

    public function setHeight(?string $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getLegend(): ?string
    {
        return $this->legend;
    }

    public function setLegend(?string $legend): self
    {
        $this->legend = $legend;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getTitleposition(): ?string
    {
        return $this->titleposition;
    }

    public function setTitleposition(?string $titleposition): self
    {
        $this->titleposition = $titleposition;

        return $this;
    }

    public function getTitletextstyle(): ?string
    {
        return $this->titletextstyle;
    }

    public function setTitletextstyle(?string $titletextstyle): self
    {
        $this->titletextstyle = $titletextstyle;

        return $this;
    }

    public function getWidth(): ?string
    {
        return $this->width;
    }

    public function setWidth(?string $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getIs3d(): ?string
    {
        return $this->is3d;
    }

    public function setIs3d(?string $is3d): self
    {
        $this->is3d = $is3d;

        return $this;
    }

    public function getSlices(): ?string
    {
        return $this->slices;
    }

    public function setSlices(?string $slices): self
    {
        $this->slices = $slices;

        return $this;
    }

    public function getPieslicebordercolor(): ?string
    {
        return $this->pieslicebordercolor;
    }

    public function setPieslicebordercolor(?string $pieslicebordercolor): self
    {
        $this->pieslicebordercolor = $pieslicebordercolor;

        return $this;
    }

    public function getSlicevisibilitythreshold(): ?string
    {
        return $this->slicevisibilitythreshold;
    }

    public function setSlicevisibilitythreshold(?string $slicevisibilitythreshold): self
    {
        $this->slicevisibilitythreshold = $slicevisibilitythreshold;

        return $this;
    }

    public function getPieresidueslicelabel(): ?string
    {
        return $this->pieresidueslicelabel;
    }

    public function setPieresidueslicelabel(?string $pieresidueslicelabel): self
    {
        $this->pieresidueslicelabel = $pieresidueslicelabel;

        return $this;
    }

    public function getBackgroundcolorStroke(): ?string
    {
        return $this->backgroundcolorStroke;
    }

    public function setBackgroundcolorStroke(?string $backgroundcolorStroke): self
    {
        $this->backgroundcolorStroke = $backgroundcolorStroke;

        return $this;
    }

    public function getBackgroundcolorStrokewidth(): ?int
    {
        return $this->backgroundcolorStrokewidth;
    }

    public function setBackgroundcolorStrokewidth(?int $backgroundcolorStrokewidth): self
    {
        $this->backgroundcolorStrokewidth = $backgroundcolorStrokewidth;

        return $this;
    }

    public function getLegendTextstyle(): ?string
    {
        return $this->legendTextstyle;
    }

    public function setLegendTextstyle(?string $legendTextstyle): self
    {
        $this->legendTextstyle = $legendTextstyle;

        return $this;
    }

    public function getReversecategories(): ?string
    {
        return $this->reversecategories;
    }

    public function setReversecategories(?string $reversecategories): self
    {
        $this->reversecategories = $reversecategories;

        return $this;
    }

    public function getTooltipShowcolorcode(): ?string
    {
        return $this->tooltipShowcolorcode;
    }

    public function setTooltipShowcolorcode(?string $tooltipShowcolorcode): self
    {
        $this->tooltipShowcolorcode = $tooltipShowcolorcode;

        return $this;
    }

    public function getTooltipTextstyle(): ?string
    {
        return $this->tooltipTextstyle;
    }

    public function setTooltipTextstyle(?string $tooltipTextstyle): self
    {
        $this->tooltipTextstyle = $tooltipTextstyle;

        return $this;
    }


}
