<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GridColModel
 *
 * @ORM\Table(name="grid_col_model")
 * @ORM\Entity
 */
class GridColModel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="col_model", type="integer", nullable=false)
     */
    private $colModel;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="index", type="string", length=150, nullable=false)
     */
    private $index;

    /**
     * @var int
     *
     * @ORM\Column(name="width", type="integer", nullable=false)
     */
    private $width;

    /**
     * @var string
     *
     * @ORM\Column(name="align", type="string", length=50, nullable=false)
     */
    private $align;

    /**
     * @var string
     *
     * @ORM\Column(name="sortable", type="string", length=10, nullable=false)
     */
    private $sortable;

    /**
     * @var string
     *
     * @ORM\Column(name="formatter", type="string", length=50, nullable=false)
     */
    private $formatter;

    /**
     * @var string
     *
     * @ORM\Column(name="datefmt", type="string", length=50, nullable=false)
     */
    private $datefmt;

    /**
     * @var string
     *
     * @ORM\Column(name="summaryType", type="string", length=50, nullable=false)
     */
    private $summarytype;

    /**
     * @var string
     *
     * @ORM\Column(name="resizable", type="string", length=50, nullable=false)
     */
    private $resizable;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getColModel(): ?int
    {
        return $this->colModel;
    }

    public function setColModel(int $colModel): self
    {
        $this->colModel = $colModel;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIndex(): ?string
    {
        return $this->index;
    }

    public function setIndex(string $index): self
    {
        $this->index = $index;

        return $this;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function setWidth(int $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getAlign(): ?string
    {
        return $this->align;
    }

    public function setAlign(string $align): self
    {
        $this->align = $align;

        return $this;
    }

    public function getSortable(): ?string
    {
        return $this->sortable;
    }

    public function setSortable(string $sortable): self
    {
        $this->sortable = $sortable;

        return $this;
    }

    public function getFormatter(): ?string
    {
        return $this->formatter;
    }

    public function setFormatter(string $formatter): self
    {
        $this->formatter = $formatter;

        return $this;
    }

    public function getDatefmt(): ?string
    {
        return $this->datefmt;
    }

    public function setDatefmt(string $datefmt): self
    {
        $this->datefmt = $datefmt;

        return $this;
    }

    public function getSummarytype(): ?string
    {
        return $this->summarytype;
    }

    public function setSummarytype(string $summarytype): self
    {
        $this->summarytype = $summarytype;

        return $this;
    }

    public function getResizable(): ?string
    {
        return $this->resizable;
    }

    public function setResizable(string $resizable): self
    {
        $this->resizable = $resizable;

        return $this;
    }


}
