<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Datasources
 *
 * @ORM\Table(name="datasources")
 * @ORM\Entity
 */
class Datasources
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=60, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="user", type="string", length=50, nullable=false)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="passwd", type="string", length=50, nullable=false)
     */
    private $passwd;

    /**
     * @var string
     *
     * @ORM\Column(name="driver", type="string", length=255, nullable=false, options={"comment"="se van a alojar en la carpeta libs"})
     */
    private $driver;

    /**
     * @var string
     *
     * @ORM\Column(name="string", type="string", length=255, nullable=false)
     */
    private $string;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=20, nullable=false, options={"comment"="[firebird,mysql,mssql,postgress]"})
     */
    private $tipo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getUser(): ?string
    {
        return $this->user;
    }

    public function setUser(string $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPasswd(): ?string
    {
        return $this->passwd;
    }

    public function setPasswd(string $passwd): self
    {
        $this->passwd = $passwd;

        return $this;
    }

    public function getDriver(): ?string
    {
        return $this->driver;
    }

    public function setDriver(string $driver): self
    {
        $this->driver = $driver;

        return $this;
    }

    public function getString(): ?string
    {
        return $this->string;
    }

    public function setString(string $string): self
    {
        $this->string = $string;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }


}
