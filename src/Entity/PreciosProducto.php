<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PreciosProducto
 *
 * @ORM\Table(name="precios_producto", uniqueConstraints={@ORM\UniqueConstraint(name="idx1", columns={"codigoparticular", "fecha"})}, indexes={@ORM\Index(name="IDX_229B9982CDECA1A", columns={"codigoparticular"})})
 * @ORM\Entity()
 */
class PreciosProducto
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="string", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="proveedor", type="string", length=100, nullable=true)
     */
    private $proveedor;

    /**
     * @var integer
     *
     * @ORM\Column(name="precio", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $precio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moneda", type="string", length=20, nullable=true)
     */
    private $moneda;

    /**
     * @var \Productos
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Productos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="codigoparticular", referencedColumnName="codigoparticular")
     * })
     */
    private $codigoparticular;

    public function getFecha(): ?string
    {
        $myDateTime = date_create_from_format('Y-m-d', $this->fecha);
        $newDateString = $myDateTime->format('d/m/y');
        return $newDateString;
    }

    public function getProveedor(): ?string
    {
        return $this->proveedor;
    }

    public function setProveedor(?string $proveedor): self
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    public function getPrecio()
    {   
        return $this->precio;
    }

    public function setPrecio($precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    public function getMoneda(): ?string
    {
        return $this->moneda;
    }

    public function setMoneda(?string $moneda): self
    {
        $this->moneda = $moneda;

        return $this;
    }

    /*public function getCodigoparticular(): ?Productos
    {
        return $this->codigoparticular;
    }*/

    public function setCodigoparticular(?Productos $codigoparticular): self
    {
        $this->codigoparticular = $codigoparticular;

        return $this;
    }


}
