<?php
    namespace App\Controller;

    use App\Entity\Aplicaciones;

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Routing\Annotation\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\Serializer\Serializer;
    use Symfony\Component\Serializer\Encoder\XmlEncoder;
    use Symfony\Component\Serializer\Encoder\JsonEncoder;
    use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

    class AplicacionController extends Controller {
        /**
         * @Route("/aplicaciones", name="lista_aplicaciones")
         * @Method({"GET"})
         */
        public function aplicaciones(){
            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            $aplicaciones= $this->getDoctrine()->getRepository(aplicaciones::class)->findAll();
            $response->setContent($serializer->serialize($aplicaciones,"json"));
            return $response;
        }
        /**
         * @Route("/producto/{id}", name="producto")
         * @Method({"GET"})
         */
        public function producto($id){
            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            $producto= $this->getDoctrine()->getRepository(Productos::class)->find($id);
            $response->setContent($serializer->serialize($producto,"json"));
            return $response;
        }
    }

