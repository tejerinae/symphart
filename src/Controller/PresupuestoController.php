<?php
    namespace App\Controller;

    use App\Entity\Presupuesto;
    use App\Entity\Clientes;
    use App\Entity\Obras;
    use App\Entity\ItemsTrans;
    use App\Entity\Aplicaciones;
    use App\Entity\Productos;

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Routing\Annotation\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\Serializer\Serializer;
    use Symfony\Component\Serializer\Encoder\XmlEncoder;
    use Symfony\Component\Serializer\Encoder\JsonEncoder;
    use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

    class PresupuestoController extends Controller {
        /**
         * @Route("/presupuestos", name="lista_presupuestos")
         * @Method({"GET", "POST"})
         */
        public function index(){
            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            $presupuestos= $this->getDoctrine()->getRepository(Presupuesto::class)->findAll();
            $response->setContent($serializer->serialize($presupuestos,"json"));
            return $response;
        }
        /**
         * @Route("/presupuesto/{idCliente}/{idObra}", name="presupuesto_control")
         * @Method({"GET", "POST"})
         */
        public function presupuesto_control($idCliente, $idObra){
            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            #$presupuesto = $this->getDoctrine()->getRepository(ItemsTrans::class)->findAllItemsbypresu($id);
            $presupuestos = $this->getDoctrine()->getRepository(Presupuesto::class)->findVersiones($idCliente,$idObra);
            #$presupuesto= $this->getDoctrine()->getRepository(Presupuesto::class)->find($id);
            $response->setContent($serializer->serialize($presupuestos,"json"));
            return $response;
        }
        /**
         * @Route("/presupuesto/{id}", name="presupuesto")
         * @Method({"GET"})
         */
        public function buscar($id){
            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            #$items = $this->getDoctrine()->getRepository(ItemsTrans::class)->findAllItemsbypresu($id);
            #$items = $this->getDoctrine()->getRepository(ItemsTrans::class)->findBy(array('idPresupuesto'=>$id, 'idPadre'=>null));
            $presupuesto= $this->getDoctrine()->getRepository(Presupuesto::class)->findOnePresubyId($id);
            #$presupuesto= $this->getDoctrine()->getRepository(Presupuesto::class)->find($id);
            $response->setContent($serializer->serialize($presupuesto,"json"));
            return $response;
        }
        /**
         * @Method({"POST","OPTIONS"})
         */
        public function guardarItems($entityManager, $presupuesto, $items, ItemsTrans $itemPadre = null){
            foreach($items as $i){
                $item = new ItemsTrans();
                $item->setIdPresupuesto($presupuesto);
                $item->setIdPadre($itemPadre);
                
                if ($i['data']['codigoparticular']){
                    $producto = $this->getDoctrine()->getRepository(Productos::class)->find($i['data']['codigoparticular']['codigoparticular']);
                    $item->setCodigoParticular($producto);
                } else {
                    $item->setCodigoParticular($i['data']['codigoparticular']);
                }

               
                if (isset($i['data']['fechaPrecio'])) {
                    /*echo gettype($i['data']['fechaPrecio']), "\n";
                    echo gettype($i['data']['tipoPrecio']), "\n";*/
                    $myDateTime = date_create_from_format('d/m/y', $i['data']['fechaPrecio']);
                    $newDateString = $myDateTime->format('Y-m-d');
                    $item->setFechaPrecio(new \DateTime($newDateString));  
                    $item->setTipoPrecio($i['data']['tipoPrecio']);
              
                } else {
                    $item->setFechaPrecio(new \DateTime('0000-00-00'));  
                    $item->setTipoPrecio('');
                };

                $item->setDescripcion($i['data']['descripcion']);
                //var_dump($i['data']['descripcion']."\n");
                $item->setCantidad($i['data']['cantidad']);
                $item->setPrecio($i['data']['precio']);
                $aplicacion = $this->getDoctrine()->getRepository(Aplicaciones::class)->find($i['data']['codigoAplicacion']);
                $item->setCodigoAplicacion($aplicacion);
                $item->setM1($i['data']['m1']);
                $item->setM2($i['data']['m2']);
                $item->setM3($i['data']['m3']);
                $item->setM4($i['data']['m4']);
                $item->setM5($i['data']['m5']);
                $item->setM5($i['data']['m5']);
                $item->setM1Desc($i['data']['m1Desc']);
                $item->setM2Desc($i['data']['m2Desc']);
                $item->setM3Desc($i['data']['m3Desc']);
                $item->setM4Desc($i['data']['m4Desc']);
                $item->setM5Desc($i['data']['m5Desc']);
                $entityManager->persist($item);
                if(count($i['children'])>0){
                    $this->guardarItems($entityManager, $presupuesto, $i['children'], $item);
                }

            }
        }

        /**
         * @Route("/presupuesto", name="guardaPresupuesto")
         * @Method({"POST","OPTIONS"})
         */
        public function guardarPresupuesto(Request $request){
           
            $data = json_decode(
                $request->getContent(),
                true
            );

            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $response->headers->set('Access-Control-Allow-Headers', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            
            $entityManager = $this->getDoctrine()->getManager();
            
            $presupuesto = new Presupuesto();
            $cliente = $this->getDoctrine()->getRepository(Clientes::class)->find($data['cliente']);
            
            /*$response->setContent($serializer->serialize($data,"json"));
            return $response;*/

            $obra = $this->getDoctrine()->getRepository(Obras::class)->find($data['obra']);

            $presupuesto->setCliente($cliente);
            $presupuesto->setObra($obra);
            $presupuesto->setVersion($data['version']);
            // tell Doctrine you want to (eventually) save the Product (no queries yet)
            $entityManager->persist($presupuesto);
            
           
            //var_dump($data['items']);
            $this->guardarItems($entityManager, $presupuesto, $data['items'] );

            // actually executes the queries (i.e. the INSERT query)
            $entityManager->flush();
            $respuesta = array('respuesta' => $presupuesto->getId());

           

            $response->setContent($serializer->serialize($respuesta,"json"));
            return $response;

            //return new Response('Saved new product with id '.$presupuesto->getId());
        }
         /**
         * @Route("/presupuesto/{cliente}/{obra}/{version}", name="borrarPresupuesto")
         * @Method("DELETE")
         */
        public function borrarPresupuesto($cliente, $obra, $version) {
            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            $presupuesto = $this->getDoctrine()->getRepository(Presupuesto::class)->findBy(array('cliente'=>$cliente, 'obra'=>$obra, 'version'=>$version));
            #$presupuesto= $this->getDoctrine()->getRepository(Presupuesto::class)->findOnePresubyId($id);
            if ($presupuesto) {
                $items = $this->getDoctrine()->getRepository(ItemsTrans::class)->findAllItemsbypresu($presupuesto[0]->getId());
                $em = $this->getDoctrine()->getManager();
                foreach($items as $item){
                    $em->remove($item);
                }
                $em->remove($presupuesto[0]);
                $em->flush();
            }
            $response->setContent($serializer->serialize(array('respuesta' => 'ok'),"json"));
            return $response;

        }

    }