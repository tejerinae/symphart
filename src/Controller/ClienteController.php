<?php
    namespace App\Controller;

    use App\Entity\Clientes;

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Routing\Annotation\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\Serializer\Serializer;
    use Symfony\Component\Serializer\Encoder\XmlEncoder;
    use Symfony\Component\Serializer\Encoder\JsonEncoder;
    use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

    class ClienteController extends Controller {
        /**
         * @Route("/clientes", name="lista_clientes")
         * @Method({"GET"})
         */
        public function clientes(){
            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            $clientes= $this->getDoctrine()->getRepository(clientes::class)->findAll();
            $response->setContent($serializer->serialize($clientes,"json"));
            return $response;
        }
        /**
         * @Route("/cliente/{nombre}", name="cliente")
         * @Method({"GET"})
         */
        public function cliente($nombre){
            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            $cliente= $this->getDoctrine()->getRepository(Clientes::class)->findClienteLike($nombre);
            $response->setContent($serializer->serialize($cliente,"json"));
            return $response;
        }
    }

