<?php
    namespace App\Controller;

    use App\Entity\Conjuntos;

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Routing\Annotation\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\Serializer\Serializer;
    use Symfony\Component\Serializer\Encoder\XmlEncoder;
    use Symfony\Component\Serializer\Encoder\JsonEncoder;
    use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

    class ConjuntoController extends Controller {
        /**
         * @Route("/conjuntospadre", name="lista_conjuntos_padre")
         * @Method({"GET"})
         */
        public function conjuntosPadre(){
            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            $conjuntos= $this->getDoctrine()->getRepository(Conjuntos::class)->findBy(array("codigoparticular"=>null));
            $response->setContent($serializer->serialize($conjuntos,"json"));
            return $response;
        }
        /**
         * @Route("/conjuntos", name="lista_conjuntos")
         * @Method({"GET"})
         */
        public function conjuntos(){
            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            $conjuntos= $this->getDoctrine()->getRepository(Conjuntos::class)->findAll();
            $response->setContent($serializer->serialize($conjuntos,"json"));
            return $response;
        }
        /**
         * @Route("/conjunto/{id}", name="conjunto")
         * @Method({"GET"})
         */
        public function conjunto($id){
            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            $conjunto= $this->getDoctrine()->getRepository(Conjuntos::class)->find($id);
            $response->setContent($serializer->serialize($conjunto,"json"));
            return $response;
        }
    }

