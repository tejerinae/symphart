<?php
    namespace App\Controller;

    use App\Entity\Obras;

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Routing\Annotation\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\Serializer\Serializer;
    use Symfony\Component\Serializer\Encoder\XmlEncoder;
    use Symfony\Component\Serializer\Encoder\JsonEncoder;
    use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

    class ObraController extends Controller {
        /**
         * @Route("/obras", name="lista_obras")
         * @Method({"GET"})
         */
        public function obras(){
            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            $obras= $this->getDoctrine()->getRepository(obras::class)->findAll();
            $response->setContent($serializer->serialize($obras,"json"));
            return $response;
        }
        /**
         * @Route("/obra/{id}", name="obra")
         * @Method({"GET"})
         */
        public function obra($id){
            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            $obras= $this->getDoctrine()->getRepository(obras::class)->findBy(array('codigocliente'=>$id));
            $response->setContent($serializer->serialize($obras,"json"));
            return $response;
        }
    }

