<?php
    namespace App\Controller;

    use App\Entity\PreciosProducto;

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Routing\Annotation\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\Serializer\Serializer;
    use Symfony\Component\Serializer\Encoder\XmlEncoder;
    use Symfony\Component\Serializer\Encoder\JsonEncoder;
    use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

    class PrecioController extends Controller {
        /**
         * @Route("/precios", name="precios")
         * @Method({"GET"})
         */
        public function precios(){
            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            $precios= $this->getDoctrine()->getRepository(PreciosProducto::class)->findAll();
            
            $response->setContent($serializer->serialize($precios,"json"));
            return $response;
        }
        /**
         * @Route("/precio/{id}", name="precio")
         * @Method({"GET"})
         */
        public function precio($id){
            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            $precio= $this->getDoctrine()->getRepository(PreciosProducto::class)->find($id);
            $response->setContent($serializer->serialize($precio,"json"));
            return $response;
        }
    }

