<?php
    namespace App\Controller;

    use App\Entity\Productos;

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Routing\Annotation\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\Serializer\Serializer;
    use Symfony\Component\Serializer\Encoder\XmlEncoder;
    use Symfony\Component\Serializer\Encoder\JsonEncoder;
    use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

    class ProductoController extends Controller {
        /**
         * @Route("/productos", name="lista_productos")
         * @Method({"GET"})
         */
        public function productos(){
            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            $productos= $this->getDoctrine()->getRepository(Productos::class)->findAll();
            $response->setContent($serializer->serialize($productos,"json"));
            return $response;
        }
        /**
         * @Route("/producto/{id}", name="producto")
         * @Method({"GET"})
         */
        public function producto($id){
            $response = new Response;
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            $producto= $this->getDoctrine()->getRepository(Productos::class)->find($id);
            $response->setContent($serializer->serialize($producto,"json"));
            return $response;
        }
    }

