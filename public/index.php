<?php

use App\Kernel;
use Symfony\Component\Debug\Debug;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Request;

require __DIR__.'/../vendor/autoload.php';

$_SERVER['APP_ENV']='dev';
#$_SERVER['DATABASE_URL']='mysql://root:W9rt85vr@127.0.0.1:3306/infocent_infotabTest';
#$_SERVER['DATABASE_URL']='mysql://root:W9rt85vr@127.0.0.1:3306/infotab';
$_SERVER['DATABASE_URL']='mysql://root:@127.0.0.1:3306/infotab';

// The check is to ensure we don't use .env in production
if (!isset($_SERVER['APP_ENV'])) {
    if (!class_exists(Dotenv::class)) {
        throw new \RuntimeException('APP_ENV environment variable is not defined. You need to define environment variables for configuration or add "symfony/dotenv" as a Composer dependency to load variables from a .env file.');
    }
    (new Dotenv())->load(__DIR__.'/../.env');
}

$env = isset($_SERVER['APP_ENV'])?$_SERVER['APP_ENV'] : 'dev';
$debug = isset($_SERVER['APP_DEBUG']) ? $_SERVER['APP_DEBUG'] : ('prod' !== $env);


if ($debug) {
    umask(0000);

    Debug::enable();
}

if ($trustedProxies = isset($_SERVER['TRUSTED_PROXIES']) ?$_SERVER['TRUSTED_PROXIES'] : false) {
    Request::setTrustedProxies(explode(',', $trustedProxies), Request::HEADER_X_FORWARDED_ALL ^ Request::HEADER_X_FORWARDED_HOST);
}

if ($trustedHosts = isset($_SERVER['TRUSTED_HOSTS'])?$_SERVER['TRUSTED_HOSTS'] : false) {
    Request::setTrustedHosts(explode(',', $trustedHosts));
}

$kernel = new Kernel($env, $debug);
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);